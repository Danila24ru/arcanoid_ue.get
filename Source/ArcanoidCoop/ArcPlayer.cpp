﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ArcPlayer.h"

#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/PawnMovementComponent.h"


// Sets default values
AArcPlayer::AArcPlayer()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void AArcPlayer::BeginPlay()
{
	Super::BeginPlay();

	//GetMovementComponent()->SetIsReplicated(true);
}

// Called every frame
void AArcPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AArcPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveRight", this, &AArcPlayer::MoveRight);
}

void AArcPlayer::Server_MoveRight_Implementation(float Value)
{
	SetActorLocation(GetActorLocation() + FVector(0.0f, 5.0f * Value, 0.0f), true);

	NetMulticast_SyncPosition(GetActorLocation());
}

void AArcPlayer::NetMulticast_SyncPosition_Implementation(FVector Position)
{
	SetActorLocation(Position);
}

void AArcPlayer::MoveRight(float Value)
{
	Server_MoveRight(Value);
}

