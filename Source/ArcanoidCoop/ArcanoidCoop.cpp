// Copyright Epic Games, Inc. All Rights Reserved.

#include "ArcanoidCoop.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ArcanoidCoop, "ArcanoidCoop" );
