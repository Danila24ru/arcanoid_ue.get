// Copyright Epic Games, Inc. All Rights Reserved.


#include "ArcanoidCoopGameModeBase.h"

#include "ArcanoidPlayerController.h"
#include "Ball.h"
#include "Kismet/GameplayStatics.h"

AArcanoidCoopGameModeBase::AArcanoidCoopGameModeBase()
{
	// Set the default values
	NumPlayers = 0;
	MaxPlayers = 2;

	TotalGameTimeSeconds = 180;
	RemainTime = 0;
	GameStartCountDownCounter = 5;
	
	DefaultPawnClass = nullptr;
	PlayerControllerClass = AArcanoidPlayerController::StaticClass();
}

void AArcanoidCoopGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	bUseSeamlessTravel = true;

	BlockSpawner = Cast<ABlockSpawner>(UGameplayStatics::GetActorOfClass(GetWorld(), ABlockSpawner::StaticClass()));

	ArcanoidGameState = Cast<AArcanoidGameState>(GameState);
	ArcanoidGameState->SetGameState(EArcanoidGameState::WaitingForPlayer);
}

void AArcanoidCoopGameModeBase::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	NumPlayers++;

	if (NumPlayers <= 2)
	{
		FTransform spawnPos = NumPlayers == 1 ?
			FTransform(FVector(0.0f, -170.0f, -260.0f)) :
			FTransform(FVector(0.0f, 170.0f, -320.0f)); 
		
		if (APawn* NewPawn = SpawnDefaultPawnAtTransform(NewPlayer, spawnPos))
		{
			NewPlayer->Possess(NewPawn);
			NewPlayer->AcknowledgePossession(NewPawn);
		}
	}

	if(NumPlayers == 2)
	{
		ArcanoidGameState->SetGameState(EArcanoidGameState::WaitingForReady);

		GetWorldTimerManager().SetTimer(GameStartCountdownTimerHandle, this, &AArcanoidCoopGameModeBase::GameStartCountdownTimerTick, 1.0f, true, 0.0f);
		
		if(IsValid(BlockSpawner))
		{
			TotalBlocks = BlockSpawner->GenerateBlocks();
			BlockSpawner->GenerateBonuses();
		}
	}
}

void AArcanoidCoopGameModeBase::GameStartCountdownTimerTick()
{
	GameStartCountDownCounter--;

	ArcanoidGameState->TimeToGameStart = GameStartCountDownCounter;
	
	if (GameStartCountDownCounter <= 0)
	{
		GetWorldTimerManager().ClearTimer(GameStartCountdownTimerHandle);
		
		// START GAME
		SetBallVelocity(FVector(0.0f, 0.0f, -500.0f));
		
		ArcanoidGameState->SetGameState(EArcanoidGameState::GameInProgress);

		GameTimerCounter = TotalGameTimeSeconds;
		GetWorldTimerManager().SetTimer(GameTimerHandle, this, &AArcanoidCoopGameModeBase::GameTimerTick, 1.0f, true, 0.0f);
	}
}


void AArcanoidCoopGameModeBase::GameTimerTick()
{
	GameTimerCounter--;

	ArcanoidGameState->GameTimer = GameTimerCounter;
	
	if(GameTimerCounter <= 0)
	{
		GetWorldTimerManager().ClearTimer(GameTimerHandle);

		ArcanoidGameState->SetGameState(EArcanoidGameState::Lose);

		BlockSpawner->ClearAllBlocks();
	}
}

void AArcanoidCoopGameModeBase::Logout(AController* Exiting)
{
	Super::Logout(Exiting);

	NumPlayers--;
}

void AArcanoidCoopGameModeBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AArcanoidCoopGameModeBase::IncTotalDestroyedBlocks()
{
	TotalDestroyedBlocks++;

	if(TotalDestroyedBlocks >= TotalBlocks)
	{
		GetWorldTimerManager().ClearTimer(GameTimerHandle);
		
		ArcanoidGameState->SetGameState(EArcanoidGameState::Win);

		SetBallVelocity(FVector(0, 0,0));
	}
}

void AArcanoidCoopGameModeBase::IncReadyPlayersForNextGame()
{
	ReadyPlayerForNextGame++;

	if(ReadyPlayerForNextGame == 2)
	{
		ReadyPlayerForNextGame = 0;
		TotalDestroyedBlocks = 0;
		NumPlayers = 2;
		TotalBlocks = 0;
		RemainTime = TotalGameTimeSeconds;
		GameStartCountDownCounter = 5;
		
		BlockSpawner->ClearAllBlocks();
		ResetBall();

		ArcanoidGameState->SetGameState(EArcanoidGameState::WaitingForReady);

		GetWorldTimerManager().SetTimer(GameStartCountdownTimerHandle, this, &AArcanoidCoopGameModeBase::GameStartCountdownTimerTick, 1.0f, true, 0.0f);
		
		if(IsValid(BlockSpawner))
		{
			TotalBlocks = BlockSpawner->GenerateBlocks();
			BlockSpawner->GenerateBonuses();
		}
	}
}

void AArcanoidCoopGameModeBase::ResetBall()
{
	auto Ball = Cast<ABall>(UGameplayStatics::GetActorOfClass(GetWorld(), ABall::StaticClass()));
	Ball->SetActorLocation(FVector(0.0f, 0.0f, 80.0f));
	Ball->SetVelocity(FVector::ZeroVector);
}

void AArcanoidCoopGameModeBase::SetBallVelocity(FVector Velocity)
{
	auto Ball = Cast<ABall>(UGameplayStatics::GetActorOfClass(GetWorld(), ABall::StaticClass()));
	Ball->SetVelocity(Velocity);
}

void AArcanoidCoopGameModeBase::IncreateVelocity(float factor)
{
	auto Ball = Cast<ABall>(UGameplayStatics::GetActorOfClass(GetWorld(), ABall::StaticClass()));
	Ball->SetVelocity(Ball->GetVelocity() * factor); 
}



