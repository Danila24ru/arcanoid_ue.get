// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ArcanoidGameState.h"
#include "BlockSpawner.h"
#include "GameFramework/GameModeBase.h"
#include "ArcanoidCoopGameModeBase.generated.h"

class ABall;
/**
 * 
 */
UCLASS()
class ARCANOIDCOOP_API AArcanoidCoopGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	
	DECLARE_EVENT_OneParam(AArcanoidCoopGameModeBase, FBlockDeathEvent, ABaseBlock*);
	FBlockDeathEvent& OnActorDeath() { return BlockDeathEvent; }

public:
	AArcanoidCoopGameModeBase();

	UPROPERTY()
	ABlockSpawner* BlockSpawner;

	UPROPERTY()
	AArcanoidGameState* ArcanoidGameState;

	virtual void BeginPlay() override;
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;
	virtual void Tick(float DeltaSeconds) override;

private:
	//Settings
	int32 NumPlayers;
	UPROPERTY(EditDefaultsOnly)
	int32 MaxPlayers;
	
	UPROPERTY(EditDefaultsOnly)
	int32 TotalGameTimeSeconds;

	// Dynamic vars
	int32 TotalDestroyedBlocks;

	int32 ReadyPlayerForNextGame;

public:
	void IncTotalDestroyedBlocks();

	UFUNCTION(BlueprintCallable)
	void IncReadyPlayersForNextGame();

	UFUNCTION(BlueprintCallable)
	void IncreateVelocity(float factor);
private:
	int32 TotalBlocks;
	int32 RemainTime;
	
	FBlockDeathEvent BlockDeathEvent;

	FTimerHandle GameStartCountdownTimerHandle;
	FTimerHandle GameTimerHandle;
	
	void GameStartCountdownTimerTick();
	void ResetBall();
	void SetBallVelocity(FVector Velocity);
	UPROPERTY()
	int GameStartCountDownCounter;
	void GameTimerTick();
	UPROPERTY()
	int GameTimerCounter;
};
