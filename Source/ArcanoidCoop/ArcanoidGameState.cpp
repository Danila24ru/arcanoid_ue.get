﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ArcanoidGameState.h"

#include "Net/UnrealNetwork.h"


// Sets default values
AArcanoidGameState::AArcanoidGameState()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AArcanoidGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AArcanoidGameState, CurrentGameState);
	DOREPLIFETIME(AArcanoidGameState, TimeToGameStart);
	DOREPLIFETIME(AArcanoidGameState, GameTimer);
	DOREPLIFETIME(AArcanoidGameState, GameScore);
	DOREPLIFETIME(AArcanoidGameState, HealthAmount);
}

void AArcanoidGameState::OnRep_GameState_Implementation()
{
}

void AArcanoidGameState::OnRep_TimeToGameStart_Implementation()
{
}

void AArcanoidGameState::OnRep_GameTimer_Implementation()
{
}

void AArcanoidGameState::OnRep_GameScore_Implementation()
{
}

void AArcanoidGameState::OnRep_HealthAmount_Implementation()
{
}

void AArcanoidGameState::SetGameState(EArcanoidGameState NewGameState)
{
	if(HasAuthority())
	{
		CurrentGameState = NewGameState;
		OnRep_GameState();
	}
}

// Called when the game starts or when spawned
void AArcanoidGameState::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AArcanoidGameState::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

