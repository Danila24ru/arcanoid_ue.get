﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "ArcanoidGameState.generated.h"

UENUM(BlueprintType)
enum class EArcanoidGameState : uint8
{
	WaitingForPlayer,
	WaitingForReady,
	GameInProgress,
	Lose,
	Win
};

UCLASS()
class ARCANOIDCOOP_API AArcanoidGameState : public AGameStateBase
{
	GENERATED_BODY()

public:
	AArcanoidGameState();

	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_GameState)
	EArcanoidGameState CurrentGameState;

	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_TimeToGameStart)
	float TimeToGameStart;

	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_GameTimer)
	float GameTimer;

	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_GameScore)
	int GameScore;

	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_HealthAmount)
	int HealthAmount;


	UFUNCTION(BlueprintNativeEvent)
	void OnRep_GameState();

	UFUNCTION(BlueprintNativeEvent)
	void OnRep_TimeToGameStart();

	UFUNCTION(BlueprintNativeEvent)
	void OnRep_GameTimer();

	UFUNCTION(BlueprintNativeEvent)
	void OnRep_GameScore();
	
	UFUNCTION(BlueprintNativeEvent)
	void OnRep_HealthAmount();
	
	UFUNCTION(BlueprintCallable)
	void SetGameState(EArcanoidGameState NewGameState);

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
