﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Ball.h"

#include "GameFramework/GameStateBase.h"
#include "Engine/NetDriver.h"

ABall::ABall()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
}

void ABall::BeginPlay()
{
	Super::BeginPlay();
}

void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if(HasAuthority())
	{
		ServerTimeCounter += DeltaTime;
		SyncTime += DeltaTime;

		if(SyncTime >= SyncInterval)
		{
			auto CurrentLocation = GetActorLocation();
			Multicast_SyncPosition(CurrentLocation, ServerTimeCounter);

			SyncTime -= SyncInterval;
		}
	}
	else
	{
		float CurrentServerTime = ServerSnapshots.Num() > 0 ? ServerSnapshots.Last().TimeStamp : 0.0f;
		
		FVector InterpolatedPosition = InterpolatePosition(CurrentServerTime, ClientLag, DeltaTime);

		SetActorLocation(InterpolatedPosition);
		
		while (ServerSnapshots.Num() > 0 && ServerSnapshots[0].TimeStamp < CurrentServerTime - 3.0f)
		{
			ServerSnapshots.RemoveAt(0);
		}
	}
}

void ABall::Multicast_SyncPosition_Implementation(FVector Position, float ServerTime)
{
	if(HasAuthority())
	{
		return;
	}
	
	ServerSnapshots.Add(FServerPositionSnapshot(Position, ServerTime));
}

FVector ABall::InterpolatePosition(const float ServerTime, const float Lag, const float DeltaTime)
{
	float InterpolationTime = GetWorld()->GetWorld()->GetGameState()->GetServerWorldTimeSeconds() - Lag;

	if(ServerSnapshots.Num() == 0)
	{
		return FVector::ZeroVector;
	}
	
	FServerPositionSnapshot PreviousSnapshot;
	FServerPositionSnapshot NextSnapshot;

	for (const FServerPositionSnapshot& Snapshot : ServerSnapshots)
	{
		if (Snapshot.TimeStamp < InterpolationTime)
		{
			PreviousSnapshot = Snapshot;
		}
		else if(Snapshot.TimeStamp > InterpolationTime)
		{
			NextSnapshot = Snapshot;
			break;
		}
	}

	if (!PreviousSnapshot.IsValid() || !NextSnapshot.IsValid())
	{
		return FVector::ZeroVector;
	}

	// Debug logs
	/*
	FString PreviousTimeStr = FString::Printf(TEXT("Previous Snapshot Time: %f"), PreviousSnapshot.TimeStamp);
	FString PreviousPosStr = FString::Printf(TEXT("Previous Snapshot Position: %s"), *PreviousSnapshot.Position.ToString());
	FString NextTimeStr = FString::Printf(TEXT("Next Snapshot Time: %f"), NextSnapshot.TimeStamp);
	FString NextPosStr = FString::Printf(TEXT("Next Snapshot Position: %s"), *NextSnapshot.Position.ToString());
	FString InterpolationTimeStr = FString::Printf(TEXT("Interpolation Time: %f"), InterpolationTime);
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, PreviousTimeStr);
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, PreviousPosStr);
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, NextTimeStr);
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, NextPosStr);
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, InterpolationTimeStr);
	*/
	
	const FVector InterpolatedPosition = PreviousSnapshot.Position +
		(NextSnapshot.Position - PreviousSnapshot.Position) *
		(InterpolationTime - PreviousSnapshot.TimeStamp) /
		(NextSnapshot.TimeStamp - PreviousSnapshot.TimeStamp);

	/*FString InterpolatedPosStr = FString::Printf(TEXT("Interpolated Position: %s"), *InterpolatedPosition.ToString());
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, InterpolatedPosStr);
	*/
	
	return InterpolatedPosition;
}

void ABall::SetVelocity(const FVector& Velocity)
{
	ProjectileMovementComponent = Cast<UProjectileMovementComponent>(GetComponentByClass(UProjectileMovementComponent::StaticClass()));
	ProjectileMovementComponent->Velocity = Velocity;
}
