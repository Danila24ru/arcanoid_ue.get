﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Ball.generated.h"

USTRUCT()
struct FServerPositionSnapshot
{
	GENERATED_BODY()
	float TimeStamp;
	FVector Position;

	FServerPositionSnapshot(): TimeStamp(0)
	{}

	FServerPositionSnapshot(const FVector& Position, const float TimeStamp)
	{
		this->Position = Position;
		this->TimeStamp = TimeStamp;
	}

	bool IsValid() const
	{
		return TimeStamp >= 0.0f;
	}
};

UCLASS()
class ARCANOIDCOOP_API ABall : public AActor
{
	GENERATED_BODY()

	UPROPERTY()
	UProjectileMovementComponent* ProjectileMovementComponent;

	UPROPERTY(EditAnywhere)
	float SyncInterval = 0.05f;

	UPROPERTY(EditAnywhere)
	float ClientLag = 0.1f;
	
	float ServerTimeCounter = 0.0f;
	float SyncTime = 0.0f;

	UPROPERTY()
	TArray<struct FServerPositionSnapshot> ServerSnapshots;

public:
	// Sets default values for this actor's properties
	ABall();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	FVector InterpolatePosition(const float ServerTime, const float Lag, const float DeltaTime);

	UFUNCTION(NetMulticast, Unreliable)
	void Multicast_SyncPosition(FVector Position, float ServerTime);

	void SetVelocity(const FVector& Velocity);
};
