﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Damageable.h"
#include "GameFramework/Actor.h"
#include "BaseBlock.generated.h"

UCLASS()
class ARCANOIDCOOP_API ABaseBlock : public AActor, public IDamageable
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	ABaseBlock();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
	int HealthAmount;

	UPROPERTY(EditAnywhere)
	bool MainBlock = true;

	virtual void HitDamage_Implementation() override;
};
