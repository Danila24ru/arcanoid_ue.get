﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockSpawner.h"


// Sets default values
ABlockSpawner::ABlockSpawner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void ABlockSpawner::BeginPlay()
{
	Super::BeginPlay();
}

int ABlockSpawner::GenerateBlocks()
{
	if (BlockClasses.Num() <= 0)
		return 0;

	int TotalBlocks = 0;
	
	for (int32 Row = 0; Row < NumRows; Row++)
	{
		for (int32 Column = 0; Column < NumColumns; Column++)
		{
			if(FMath::RandBool())
			{
				continue;
			}
			
			TotalBlocks++;
			
			FVector SpawnLocation = GetActorLocation() + FVector(0.0f, Column * BlockSpacing, Row * BlockSpacing);

			int32 RandomBlockIndex = FMath::RandRange(0, BlockClasses.Num() - 1);
				
			ABaseBlock* NewBlock = GetWorld()->SpawnActor<ABaseBlock>(BlockClasses[RandomBlockIndex], SpawnLocation, FRotator::ZeroRotator);

			SpawnedBlocks.Add(NewBlock);
		}
	}

	return TotalBlocks;
}

int ABlockSpawner::GenerateBonuses()
{
	for(int i = 0; i < BonusClasses.Num(); i++)
	{
		FVector SpawnLocation = GetActorLocation() + FVector(0.0f, FMath::RandRange(-50.0f, 50.0f), FMath::RandRange(0.0f, 30.0f));

		ABaseBlock* NewBlock = GetWorld()->SpawnActor<ABaseBlock>(BonusClasses[i], SpawnLocation, FRotator::ZeroRotator);
	}

	return BonusClasses.Num();
}

void ABlockSpawner::ClearAllBlocks()
{
	SpawnedBlocks.Empty();
}
