﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseBlock.h"
#include "GameFramework/Actor.h"
#include "BlockSpawner.generated.h"

UCLASS()
class ARCANOIDCOOP_API ABlockSpawner : public AActor
{
	GENERATED_BODY()

public:
	ABlockSpawner();

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere, Category = "Block Generation")
	TArray<TSubclassOf<ABaseBlock>> BlockClasses;

	UPROPERTY(EditAnywhere, Category = "Bonus Generation")
	TArray<TSubclassOf<ABaseBlock>> BonusClasses;

	UPROPERTY()
	TArray<ABaseBlock*> SpawnedBlocks;

	UPROPERTY(EditAnywhere, Category = "Block Generation")
	int32 NumRows;

	UPROPERTY(EditAnywhere, Category = "Block Generation")
	int32 NumColumns;

	UPROPERTY(EditAnywhere, Category = "Block Generation")
	float BlockSpacing;

public:
	UFUNCTION(BlueprintCallable)
	int GenerateBlocks();

	UFUNCTION(BlueprintCallable)
	int GenerateBonuses();

	UFUNCTION(BlueprintCallable)
	void ClearAllBlocks();
};
